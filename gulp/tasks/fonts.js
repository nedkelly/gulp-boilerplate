
"use strict";

// Fonts

const config   = require('../config');
const gulp     = require('gulp');
const notify   = require('gulp-notify');
const plumber  = require('gulp-plumber');
const utility  = require('../utility');


/**
 * Images - copy from source to build directory and compress.
 */
gulp.task('fonts', function() {
  return gulp.src(config.fonts.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(gulp.dest(config.fonts.output))
    .pipe(notify({
      title: 'Fonts',
      message: '✔ fonts copied successfully.',
      onLast: true
    }));
});
