var gulp    = require('gulp');
const config   = require('../config');
var sassdoc = require('sassdoc');

gulp.task('sassdoc', function () {
  var options = {
    dest: config.css.docs,
    theme: 'neat',
    verbose: true,
    basePath: 'https://github.com/SassDoc/sassdoc',
  };

  console.log(config.css.input);

  return gulp.src(config.css.input)
    .pipe(sassdoc(options));
});
