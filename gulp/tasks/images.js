
"use strict";

// Images

const config   = require('../config');
const gulp     = require('gulp');
const notify   = require('gulp-notify');
const plumber  = require('gulp-plumber');
const utility  = require('../utility');


/**
 * Images - copy from source to build directory and compress.
 */
gulp.task('images', function() {
  return gulp.src(config.images.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(gulp.dest(config.images.output))
    .pipe(notify({
      title: 'Images',
      message: '✔ images copied successfully.',
      onLast: true
    }));
});
