
"use strict";

// Javascript Task

const args       = require('yargs').argv;
const browserify = require('browserify');
const buffer     = require('gulp-buffer');
const config     = require('../config');
const gulp       = require('gulp');
const gulpif     = require('gulp-if');
const header     = require('gulp-header');
const notify     = require('gulp-notify');
const pkg        = require('../../package.json');
const plumber    = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const tap        = require('gulp-tap');
const uglify     = require('gulp-uglify');
const utility    = require('../utility');

/**
 * Reusable Gulp task for JS concat/minification.
 *
 * @param  {string}  category Name of config object from gulp/config.js
 * @param  {boolean} dev      Flag whether the --dev flag is present.
 */
var jsTask = function(category, dev) {
  return gulp.src(config.js[category].input, { read: false })
    .pipe(plumber({ errorHandler: utility.errorHandler }))

    // Transform file objects using gulp-tap plugin
    .pipe(tap(function (file) {

      // Replace file contents with browserify's bundle stream
      file.contents = browserify(file.path, {
        debug: true
      })
      .transform("babelify", {
        presets: ["es2015"]
      })
      .bundle();

    }))

    // Transform streaming contents into buffer contents (because gulp-sourcemaps does not support streaming contents)
    .pipe(buffer())

    // Load and init sourcemaps
    .pipe(gulpif(!dev, sourcemaps.init({ loadMaps: true })))

    // Uglify
    .pipe(gulpif(!dev, uglify()))

    // Add Banner
    .pipe(header(config.banner, {
      pkg: pkg,
      timestamp: Math.floor(Date.now() / 1000)
    }))

    // Write sourcemaps
    .pipe(gulpif(!dev, sourcemaps.write('./')))

    // Write dest
    .pipe(gulp.dest(config.js.output))
    .pipe(notify({
      title: 'JavaScript',
      message: '✔ ' + config.js[category].filename + ' compiled successfully.',
      onLast: true
    }));
};



gulp.task('js:main', function() {
  let dev = args.dev === true;
  return jsTask('main', dev);
});

gulp.task('js:libraries', function() {
  let dev = args.dev === true;
  return jsTask('libraries', dev);
});

gulp.task('js:polyfills', function() {
  let dev = args.dev === true;
  return jsTask('polyfills', dev);
});

gulp.task('js', ['js:main', 'js:libraries', 'js:polyfills']);
