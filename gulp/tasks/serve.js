
"use strict";

// Serve

const bs       = require('browser-sync');
const config   = require('../config');
const gulp     = require('gulp');
const notify   = require('gulp-notify');
const plumber  = require('gulp-plumber');
const ps       = require('project-server');
const utility  = require('../utility');

/**
 * Serve task - runs a server allowing access to the files.
 */
gulp.task('serve', function() {
  bs.init({
    files: config.serve.input + '**',
    server: {
      baseDir: config.serve.input,
      directory: false,
      middleware: [ps.projectServerIndex(config.serve.input, { config: config.serve.projectServer })],
    },
    notify: false,
    online: false,
    open: "local"
  });
});
