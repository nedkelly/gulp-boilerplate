
"use strict";

// Documentation

const config   = require('../config');
const flatten  = require('gulp-flatten');
const gulp     = require('gulp');
const gutil    = require('gulp-util');
const markdown = require('gulp-markdown');
const notify   = require('gulp-notify');
const pkg      = require('../../package.json');
const plumber  = require('gulp-plumber');
const utility  = require('../utility');
const wrap     = require('gulp-wrap');

/**
 * Docs tasks - generates html documentation.
 */
gulp.task('docs', function() {
  return gulp.src(config.docs.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(flatten())
    .pipe(markdown())
    .pipe(wrap({
      src: config.docs.template
    },
    {
      version: pkg.version,
      favicon: config.docs.favicon
    }))
    .pipe(gulp.dest(config.docs.output))
    .pipe(notify({
      title: 'Documentation',
      message: '✔ documentation updated successfully.',
      onLast: true
    }));
});
