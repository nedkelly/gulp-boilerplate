
"use strict";

// Default

const args     = require('yargs').argv;
const gulp     = require('gulp');


/**
 * Default Gulp task, checks that the Swigger dependencies are installed before continuing.
 */
gulp.task('default', function() {
  let tasks = [];

  if (args.build !== false) {
    tasks.push('build');
  }

  tasks.push('serve');
  tasks.push('watch');

  gulp.start(tasks);
});
