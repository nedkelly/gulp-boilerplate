
"use strict";

// Package

const config   = require('../config');
const gulp     = require('gulp');
const notify   = require('gulp-notify');
const pkg      = require('../../package.json');
const prompt   = require('gulp-prompt');
const utility  = require('../utility');
const zip      = require('gulp-zip');


/**
 * Package task
 */
gulp.task('package', function() {
  return gulp.src(config.package.input)
    .pipe(prompt.confirm({
      message: 'You are about to export the current build as ' + config.package.output + pkg.version + '.zip, continue?',
      default: true
    }))
    .pipe(zip(pkg.version + '.zip'))
    .pipe(gulp.dest(config.package.output))
    .pipe(notify({
      title: 'Package',
      message: '✔ JavaScript compiled successfully.',
      onLast: true
    }));
});
