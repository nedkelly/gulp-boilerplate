
"use strict";

// Swig

const config   = require('../config');
const gulp     = require('gulp');
const gutil    = require('gulp-util');
const merge    = require('merge-stream');
const notify   = require('gulp-notify');
const path     = require('path');
const plumber  = require('gulp-plumber');
const swig     = require('gulp-swig');
const tap      = require('gulp-tap');
const utility  = require('../utility');
const options  = {
  defaults: {
    cache: false,
    varControls: ['{=', '=}']
  },
  load_json: true,
  json_path: './src/data/'
};


/**
 * Templates task - compiles swig.
 */
gulp.task('swig', function() {

  // Merge stream allows us to output to multiple destinations
  let stream,
      name = '',
      folders = [];

  config.swig.folders.forEach( function(folder) {

    // Create Stream
    stream = gulp.src(config.swig.input + folder.input + '**/*.swig')
      .pipe(plumber({ errorHandler: utility.errorHandler }))
      .pipe(swig(options))
      .pipe(tap( function(file) {
        name = '/' + path.basename(file.path);
      }))
      .pipe(gulp.dest(config.swig.output + folder.output + name));

    // Push streams to folders
    folders.push(stream);
  });

  // Merge streams
  return merge(folders)
    .pipe(notify({
      title: 'Templates',
      message: '✔ Template files compiled successfully.',
      onLast: true
    }));

});
