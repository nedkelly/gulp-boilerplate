
"use strict";

// Watch

const config   = require('../config');
const gulp     = require('gulp');
const watch    = require('gulp-watch');


/**
 * Gulp watch task.
 */
gulp.task('watch', function() {
  config.watch.tasks.forEach(function(t) {
    watch(t.path, function() {
      gulp.start(t.task);
    });
  });
});
