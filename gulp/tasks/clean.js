
"use strict"

// Clean

const config   = require('../config');
const del      = require('del');
const gulp     = require('gulp');


/**
 * Gulp clean - deletes all the build files.
 */
gulp.task('clean', function(cb) {
  return del(config.clean.input, cb);
});
