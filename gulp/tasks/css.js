
"use strict"

// CSS

const args     = require('yargs').argv;
const bourbon  = require('node-bourbon');
const combine  = require('gulp-combine-mq');
const config   = require('../config');
const cssmin   = require('gulp-cssmin');
const gulp     = require('gulp');
const gulpif   = require('gulp-if');
const header   = require('gulp-header');
const notify   = require('gulp-notify');
const pkg      = require('../../package.json');
const plumber  = require('gulp-plumber');
const sass     = require('gulp-sass');
const utility  = require('../utility');


/**
 * CSS - compile stylesheets.
 */
gulp.task('css', function() {
  let dev = args.dev === true;

  return gulp.src(config.css.input)
    .pipe(plumber({ errorHandler: utility.errorHandler }))
    .pipe(sass({
      outputStyle: 'expanded',
      includePaths: bourbon.includePaths
    }).on('error', sass.logError))
    .pipe(combine({
      beautify: false
    }))
    .pipe(gulpif(!dev, cssmin()))
    .pipe(header(config.banner, {
      pkg: pkg,
      timestamp: Math.floor(Date.now() / 1000)
    }))
    .pipe(gulp.dest(config.css.output))
    .pipe(notify({
      title: 'CSS',
      message: '✔ CSS compiled successfully.',
      onLast: true
    }));
});
