
"use strict";

const pkg    = require('../package.json');
const pubDir = "./public/";
const srcDir = "./src/";


/**
 * JavaScript configuration object
 *
 * @type {Object}
 */
module.exports.js = {
  main: {
    input: srcDir + 'assets/js/main.js',
    filename: 'main.js',
    watch: srcDir + 'assets/js/main/**/*.js'
  },
  libraries: {
    input: srcDir + 'assets/js/libraries.js',
    filename: 'libraries.js',
    watch: srcDir + 'assets/js/libraries/**/*.js'
  },
  polyfills: {
    input: srcDir + 'assets/js/polyfills.js',
    filename: 'polyfills.js',
    watch: srcDir + 'assets/js/polyfills/**/*.js'
  },
  output: pubDir + 'assets/js/'
};


/**
 * CSS configuration object.
 *
 * @type {Object}
 */
module.exports.css = {
  input:  srcDir + 'assets/css/**/*.scss',
  output: pubDir + 'assets/css/',
  docs: pubDir + 'sassdocs/',
  watch: srcDir + 'assets/css/**/*.scss'
};


/**
 * Swig configuration object.
 *
 * @type {Object}
 */
module.exports.swig = {
  folders: [
    { input: 'modules/', output: 'modules/', recursive: false },
    { input: 'pages/', output: '/', recursive: false }
  ],
  input: srcDir + '',
  output: pubDir + '',
  watch: srcDir + '**/*.swig'
};


/**
 * Images configuration object.
 *
 * @type {Object}
 */
module.exports.images = {
  input:  srcDir + 'assets/img/**/*',
  output: pubDir + 'assets/img/',
  watch:  srcDir + 'assets/img/**/*'
};


/**
 * Images configuration object.
 *
 * @type {Object}
 */
module.exports.fonts = {
  input:  srcDir + 'assets/fonts/**/*',
  output: pubDir + 'assets/fonts/',
  watch:  srcDir + 'assets/fonts/**/*'
};


/**
 * Clean-up configuration. What to remove when running `gulp clean`.
 *
 * @type {Object}
 */
module.exports.clean = {
  input: [
    pubDir + '**/*'
  ]
};


/**
 * Package configuration object.
 *
 * @type {Object}
 */
module.exports.package = {
  input: pubDir + '**',
  output: './package/'
};


/**
 * Serve configuration object.
 *
 * @type {Object}
 */
module.exports.serve = {
  input: pubDir + '',
  projectServer: {
    //defaultPage: '/docs/README.html',
    publicDir: pubDir,
    localStorageSeed: 'my cat is fat',
    menus: [
      {
        title: 'Pages',
        path: '/'
      },
      {
        title: 'Modules',
        path: '/modules'
      },
      {
        title: 'Documentation',
        path: '/docs'
      }
    ],
    pkg: {
      name: pkg.name,
      favicon: '/assets/img/charlie.jpg',
      version: pkg.version,
    }
  }
};


/**
 * Docs configuration object.
 *
 * @type {Object}
 */
module.exports.docs = {
  input: [
    './*.md',
    srcDir + '**/*.md'
  ],
  output: pubDir + 'docs/',
  template: './gulp/templates/docs.html',
  favicon: '../img/charlie.jpg'
};

/**
 * Watch configuration object.
 *
 * @type {Object}
 */
module.exports.watch = {
  tasks: [
    { path: module.exports.js.main.watch,      task: ['js:main'] },
    { path: module.exports.js.libraries.watch, task: ['js:libraries'] },
    { path: module.exports.js.polyfills.watch, task: ['js:polyfills'] },
    { path: module.exports.css.watch,          task: ['css'] },
    { path: module.exports.images.watch,       task: ['images'] },
    { path: module.exports.fonts.watch,        task: ['fonts'] },
    { path: module.exports.swig.watch,         task: ['swig'] },
    { path: module.exports.docs.input,         task: ['docs'] }
  ]
};


/**
 * Banner string to be placed above JS and CSS files.
 *
 * @type {String}
 */
module.exports.banner = [
  '/**',
  ' * <%= pkg.name %>',
  ' * @version v<%= pkg.version %>',
  ' * @timestamp <%= timestamp %>',
  ' */',
  ''
].join('\n');
