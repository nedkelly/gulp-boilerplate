# Gulp Boilerplate

Styleguide and codebase for all client-side code.

### What's in this build?

- [GulpJS](http://gulpjs.com/)
- [Swig](http://paularmstrong.github.io/swig/)
- [LibSass](http://libsass.org/) with [Bourbon mixin library](http://bourbon.io/)
- [Browsersync](http://www.browsersync.io/) with [Project Server](https://www.npmjs.com/package/project-server)

### Getting started

To get started, simply follow these simple steps:

###### 1. Clone the repository

``` bash
git clone git@bitbucket.org:nedkelly/gulp-boilerplate.git
cd gulp-boilerplate
```

###### 2. Install dependencies

``` bash
npm install -g gulp
npm install
```

###### 3. Run the default gulp task

``` bash
gulp
```

### Available Gulp tasks

Run `gulp` followed by one of the tasks below to perform a specific action. Otherwise run `gulp` by itself to run the default task defined below.

- **build** - runs [js, css, images, fonts, templates]
- **rebuild** - runs [clean, build]
- **clean** - removes content of `./public` directory
- **css** - compiles SASS and minifies the source
- **default** - runs [build, serve, watch]
- **images** - copies images from `./src/assets/img/` to `./public/assets/img/`
- **fonts** - copies fonts from `./src/assets/fonts/` to `./public/assets/fonts/`
- **js** - concatenates and minifies JS files
- **serve** - runs a local server from the `./public/` directory and reloads the browser if changes are made
- **templates** - compiles swig templates into pages
- **watch** - watches js, css, images and templates and runs correspondent task when a file is modified/created
- **package** - zips up the public directory into the current version defined in package.json
- **docs** - generate the documentation based on the markdown files in the `src` directory.

### Gulp flags

- `gulp --dev` - Does not minify JS/CSS assets.
- `gulp --no-build` - Only watches and serves - does not perform the initial build.
